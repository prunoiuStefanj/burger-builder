import React, { Component } from 'react';

import Auxiliary from '../../../hoc/Auxiliary/Auxiliary';
import Button from '../../UI/Button/Button';

class OrderSummary extends Component {
    //This could be a functional component, doeesn't have to be a class
    componentWillUpdate() {
        console.log('[OrderSummary] update');
    }
    render(){
        const ingredientsSummary = Object.keys(this.props.ingredients)
            .map(igKey => {
                return (
                    <li key={igKey}>
                        <span style={{textTransform: 'capitalize'}}>{igKey} </span> :
                            {this.props.ingredients[igKey]}
                    </li>)
            });
        
        return(
            <Auxiliary>
                <h3>Your order</h3>
                <p>A delicious burger with the following ingredients:</p>
                <ul>
                    {ingredientsSummary}
                </ul>
                <p><strong> Total price: {this.props.price.toFixed(2)}</strong></p>
                <p>Continue to Checkout? </p>
                <Button btnType="Danger" clicked={this.props.purchaseCancelled}>CANCEL</Button>
                <Button btnType="Success" clicked={this.props.purchaseCountinue}>CONTINUE</Button>
            </Auxiliary>
        );
    }
};

export default OrderSummary;